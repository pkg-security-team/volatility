volatility (2.6.1-1) unstable; urgency=medium

  * New upstream version 2.6.1.
  * debian/copyright: updated copyright years for Michael Hale Ligh.
  * debian/manpage/*: updated manpage.
  * debian/tests/control: using Restriction field instead of '2>'.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 21 Nov 2018 00:31:23 -0200

volatility (2.6+git20170711.b3db0cc-2) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * Migrated DH level to 11.
  * debian/control:
      - Bumped Standards-Version to 4.2.1.
      - Removed X-Python-Version field.
  * debian/copyright:
      - Added rights for Raphaël Hertzog.
      - Updated packaging copyright years.
      - Using a secure copyright format in URI.
  * debian/tests/control: created to perform a trivial test.

  [ Raphaël Hertzog ]
  * debian/control:
      - Changed Vcs-* URLs to salsa.debian.org.
      - Updated team maintainer address to Debian Security Tools
        <team+pkg-security@tracker.debian.org>.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 19 Sep 2018 21:53:13 -0300

volatility (2.6+git20170711.b3db0cc-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
      - Bumped Standards-Version to 4.0.0.
      - Replaced python-imaging with python-pil in Depends field.
        Thanks to Matthias Klose <doko@debian.org> (Closes: #866492)
  * debian/copyright: updated the upstream copyright years.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 19 Jul 2017 20:03:25 -0300

volatility (2.6-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: updated the description.
  * debian/copyright: updated the copyright packaging years.
  * debian/manpage/: updated all files to generate an updated 2.6 manpage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 01 Jan 2017 18:52:44 -0200

volatility (2.5+git20161224.736bc3a-1) unstable; urgency=medium

  * New upstream release.
  * debian/rules: added a rm command in override_dh_auto_install target to
    remove the doxygen directory.
  * debian/source/lintian-overrides: removed. The upstream no longer uses the
    d3.js file directly.
  * debian/watch: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 25 Dec 2016 22:35:44 -0200

volatility (2.5+git20161121.ecd8a54-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 10 Dec 2016 00:02:28 -0200

volatility (2.5+git20161026.75fb034-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/10_python-macos-interpreter.patch: removed. The upstream
    fixed the source code. Thanks.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 30 Oct 2016 11:58:56 -0200

volatility (2.5+git20161021.19d1211-1) unstable; urgency=medium

  * New upstream release. This release fixes partially an issue with Kernel
    Linux 4.7. (see #839754)
  * Bumped DH level to 10.
  * Using GitHub project page as official upstream homepage.
  * debian/control: updated the long description for volatility.
  * debian/copyright: updated some upstream copyright dates.
  * debian/manpages:
      - Changed from genallman.sh to create-man.sh.
      - Updated manpage as '2.6-pre' version.
  * debian/patches/10_python-macos-interpreter.patch: added to provides an
    interpreter for python in MacOs.
  * debian/rules:
      - Removed the --parallel option from dh.
      - Removed the override_dh_auto_build target.
  * debian/source/lintian-overrides: added to override a lintian mistake.
  * debian/volatility-tools.README.Debian: updated.
  * debian/watch: added a dversionmangle to ignore the current Git version.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 22 Oct 2016 13:02:46 -0200

volatility (2.5-2) unstable; urgency=medium

  * debian/control:
      - Bumped Standards-Version to 3.9.8.
      - Fixed the name "openSUSE" in long description.
      - Updated the Vcs-* fields to use https instead of http and git.
  * debian/copyright: updated the packaging copyright years.
  * debian/manpage/: updated the manpage. (Closes: #824438)
  * debian/watch: bumped to version 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 07 Aug 2016 18:54:34 -0300

volatility (2.5-1) unstable; urgency=medium

  * New upstream release.
  * debian/clean: not needed. Removed.
  * debian/control: updated the long description.
  * debian/copyright:
      - The upstream's README.txt says GPL-2+. So, updated the
        license in debian/copyright.
      - Relicensed the packaging to be compliant with upstream.
      - Updated all information.
  * debian/gbp.conf: not used by me... Removed.
  * debian/manpage/:
      - Updated the manpage.
      - Updated the genallman.sh to v0.3.
  * debian/source/options: not needed. Removed.
  * debian/volatility.docs: added AUTHORS.txt and CREDITS.txt.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 21 Nov 2015 12:01:43 -0200

volatility (2.4.1-2) unstable; urgency=medium

  * Upload to unstable. Welcome Jessie Stable.
  * debian/control: fixed the extra spaces between lines. Thanks to
      Davide Prina <davide.prina@gmail.com> (Closes: #768775)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 29 Apr 2015 12:57:04 -0300

volatility (2.4.1-1) experimental; urgency=medium

  * New upstream release.
  * debian/copyright:
      - Removed the block 'Files: tools/linux/pmem/pmem.c'. The pmem no longer
        exists in Volatility.
      - Removed not used 'Apache-2.0' license text.
      - Updated the packaging copyright years.
  * debian/man/:
      - Little adjustments in manpage.
      - Renamed to debian/manpage/.
  * debian/rules: added the override_auto_clean target to remove some files
      forgotten by upstream.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 23 Feb 2015 14:02:52 -0300

volatility (2.4-4) unstable; urgency=medium

  * Upload to unstable.
  * debian/control: removed the Recommends field because volatility-profiles
      no longer exists in unstable/testing (see #766895).

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 27 Nov 2014 23:17:36 -0200

volatility (2.4-3) experimental; urgency=medium

  * debian/copyright: added a new upstream site. See below.
  * debian/watch: The Volatility Project replied me a recent email
       message and the development site (GitHub) now uses tags.
       Thanks a lot to Jamie Levy (gleeda).

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 20 Nov 2014 19:09:46 -0200

volatility (2.4-2) experimental; urgency=medium

  * debian/watch: added a fake site to explain about the current
      status of the original upstream homepage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 18 Nov 2014 08:45:16 -0200

volatility (2.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/clean: added to remove some files generated by upstream when
      building.
  * debian/control:
      - Added dh-python as build dependency.
      - Added python-distorm3 and python-tz as install dependencies
        to volatility binary.
      - Fixed the name 'lime-forensics-dkms' in Suggests field.
      - Following the upstream README, changed X-Python-Version from
        >= 2.7 to 2.7.
      - Improved the long description.
      - Removed the volatility-profiles, a recommended package, from
        volatility binary. This package is dead and will be removed
        from Debian.
      - Updated the Standards-Version from 3.9.5 to 3.9.6.
  * debian/copyright:
      - Updated the Source field.
      - Updated the upstream names and copyright years.
  * debian/man/:
      - Changed the generator script from genman.sh to genallman.sh.
      - Removed (now) useless file 'notes'.
      - Updated the manpage.
  * debian/volatility-tools.README.Debian: improved.
  * debian/watch: deactivated because the new upstream site is using
      resources that can't be monitored.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 25 Oct 2014 17:15:53 -0300

volatility (2.3.1-10) unstable; urgency=medium

  * New maintainer email address.
  * debian/control: updated the Vcs-Browser field.
  * debian/man/:
      - Added genman.sh to automate the manpage creation.
      - Renamed volatility.1.header to header.txt.
  * debian/volatility-tools.dirs: removed because the
    volatility-tools.install file already creates the
    directory.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 08 Aug 2014 13:45:27 -0300

volatility (2.3.1-9) unstable; urgency=medium

  * debian/volatility-tools.README.Debian: updated the information about
    the new profile folder.

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Mon, 31 Mar 2014 20:30:41 -0300

volatility (2.3.1-8) unstable; urgency=medium

  * debian/control: fixed the Vcs-Git field. Thanks to
    Mario Lang <mlang@debian.org> for report.
  * debian/watch: improved.

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Fri, 21 Feb 2014 08:29:47 -0300

volatility (2.3.1-7) unstable; urgency=medium

  * debian/control: moved python from Depends to Suggests field in
    volatility-tools binary, to avoid unnecessary installs when
    making a Linux profile only. It is a special case.

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Fri, 31 Jan 2014 07:40:07 -0200

volatility (2.3.1-6) unstable; urgency=medium

  * debian/control: removed minimum python version from volatility-tools,
    to allow the profile creation on old versions of the distributions.
  * debian/volatility.lintian-overrides: useless; removed.

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Thu, 30 Jan 2014 22:34:47 -0200

volatility (2.3.1-5) unstable; urgency=medium

  * debian/control:
      - Added python as dependency in volatility-tools binary.
      - Changed the minimum python version from 2.6 to 2.7 in
        X-Python-Version field.
  * debian/*.install: added to create the volatility and volatility-tools
        packages.
  * debian/rules:
      - Changed in python setup line from --root=debian/volatility to
        --root=debian/tmp.
      - Removed the lines used to create the volatility-tool package.
        This is made by debian/*.install files now.
      - Removed the DESTDIR* lines.

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Thu, 30 Jan 2014 14:12:34 -0200

volatility (2.3.1-4) unstable; urgency=high

  * debian/control: changed yara to python-yara as volatility dependency.

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Sun, 26 Jan 2014 16:42:27 -0200

volatility (2.3.1-3) unstable; urgency=medium

  * Updated to unstable.
  * debian/control: updated the long description.

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Fri, 17 Jan 2014 08:11:48 -0200

volatility (2.3.1-2) experimental; urgency=medium

  * New binary:
      - Created volatility-tools to provide, separately, the code used to
        generate profiles to Volatility.
      - Added the volatility-tools.dirs file to provides
        /usr/src/volatility-tools.
      - Added a README.Debian to talk about the profiles creation process.
      - Renamed debian/docs to debian/volatility.docs; debian/links to
        volatility.links; manpages to volatility.manpages.
      - Updated the debian/rules file.
  * debian/control:
      - Added volatility-tools and yara as volatility dependency.
      - Added volatility-profiles as volatility recommendation.
      - Bumped Standards-Version from 3.9.4 to 3.9.5.
  * debian/copyright:
      - Added Michael Prokop to maintainers.
      - Updated the packaging years.
  * debian/watch: improved.

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Tue, 07 Jan 2014 15:36:52 -0200

volatility (2.3.1-1) unstable; urgency=low

  * Initial release (Closes: #728251)

 -- Joao Eriberto Mota Filho <eriberto@eriberto.pro.br>  Sat, 02 Nov 2013 01:10:33 -0200
